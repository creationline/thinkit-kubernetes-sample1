# README.md

## Project layout

- `deployment`

  Deployment manifest for develop/production release

- `package`

  Manifests for packaging (ex. `Dockerfile`) for develop/production release

- `scripts`

  Script files for build, run, and other development tasks.

- `src`

  Subprojects for application code.

## Architecture

- High level architecture

  ```mermaid
  graph TD
    browser["Browser"]
    browser --> article
    browser --> website
    subgraph k8s["Kubernetes"]
      website -. "SSR(*)" .-> article["Article"]
      website -. "SSR(*)" .-> rank["Rank"]
      subgraph websitesvc["Web Site Service"]
        website["Web Site"]
      end
      article -- "update access count" --> accesscount["AccessCount"]
      rank -- "get access count" --> accesscount
      subgraph articlesvc["Article Service"]
        article -- "get article" --> articledb[("Article DB")]
      end
      subgraph accesscountsvc["AccessCount Service"]
        accesscount -- "get/update access count" --> accesscountdb[("AccessCount DB")]
      end
      subgraph ranksvc["Rank Service"]
        rank -- "get/update ranking" --> rankdb[("Rank DB")]
      end
      rank --> article
    end
    browser --> rank
  ```

- Kubernetes resources

  ```mermaid
  graph TD
    browser[Browser] --> articleIng
    browser --> rankIng
    browser --> websiteIng
    subgraph cluster["Kubernetes cluster"]
      articlePod --> accesscountSvc
      rankPod --> accesscountSvc
      websitePod -. "SSR(*)" .-> articleSvc
      subgraph websiteApp["app: Website"]
        websiteIng(["ing/website"]) --> websiteSvc(["svc/website"])
        websiteSvc --> websitePod
        subgraph websiteDeploy["deploy/website"]
          subgraph websiteRs["rs/website-*"]
            websitePod(["pod/website-*"])
          end
        end
      end
      subgraph articleApp["app: Article"]
        articleIng(["ing/article"]) --> articleSvc
        articleSvc(["svc/article"]) --> articlePod
        articlePod --> articleDBSvc
        subgraph articleDeploy["deploy/article"]
          subgraph articleRs["rs/article-*"]
            articlePod(["pod/article-*"])
          end
        end
        articleDBSvc(["svc/articledb"]) --> articleDBPod
        subgraph articleDBSts["sts/articledb"]
          articleDBPod(["pod/articledb-*"])
        end
      end
      subgraph accesscountApp["app: AccessCount"]
        accesscountInt(["ing/accesscount"]) --> accesscountSvc
        accesscountSvc(["svc/accesscount"]) --> accesscountPod
        accesscountPod --> accesscountDBSvc
        subgraph accesscountDeploy["deploy/accesscount"]
          subgraph accesscountRs["rs/accesscount-*"]
            accesscountPod(["pod/accesscount-*"])
          end
        end
        accesscountDBSvc(["svc/accesscountdb"]) --> accesscountDBPod
        subgraph accesscountDBSts["sts/accesscountdb"]
          accesscountDBPod(["pod/accesscountdb-*"])
        end
      end
      subgraph rankApp["app: Rank"]
        rankIng(["ing/rank"]) --> rankSvc
        rankSvc(["svc/rank"]) --> rankPod
        rankPod --> rankDBSvc
        subgraph rankDeploy["deploy/rank"]
          subgraph rankRs["rs/rank-*"]
            rankPod(["pod/rank-*"])
          end
        end
        rankDBSvc(["svc/rankdb"]) --> rankDBPod
        subgraph rankDBSts["sts/rankdb"]
          rankDBPod(["pod/rankdb-*"])
        end
      end
      rankPod --> articleSvc
      websitePod -. "SSR(*)" .-> rankSvc
    end
  ```

## Develop

> Replace `${component}` to component name: `article`, `accesscount`, or `rank`.

### Develop with docker (Recommended)

- Build container images for development:

  ```bash
  ./scripts/build-develop-docker.sh "${component}"
  ```

  This script build and update container images named `${component}:dev-docker`

  `dev-docker` container optimized only for development. The image runs application with `mvn spring-boot:run` command, so application runs with `devtools`. `devtools` enables "hot reload" that provide great experience for development. But you should not run it public environment.

- Run container:

  ```bash
  ./scripts/run-develop-docker.sh "${component}"
  ```

- Or run all containers:

  ```bash
  ./scripts/docker-compose.sh up -d
  ```

### Develop with local Kubernetes

Run with on-premise kubernetes (constructed by `kubeadm`), `kind`, `minikube`, and kubernetes bundled with docker-desktop.

- Build container images for development

  ```bash
  REPOSITORY=<repo URL> ./scripts/build-develop.sh "${component}"
  ```

  - Build container image will be tagged: `<repo URL>/<component>:dev`

- Setup `kubectl` command to manipulate target kubernetes cluster:

  ```text
  $ kubectl cluster-info
  Kubernetes control plane is running at <target kubernetes apiserver endpoint>
  KubeDNS is running at <target kubernetes apiserver endpoint>/api/v1/namespaces/kube-system/services/kube-dns:dns/proxy

  To further debug and diagnose cluster problems, use 'kubectl cluster-info dump'.
  ```

- Run containers:

  ```bash
  kubectl apply -k ./deployment/develop/kubernetes/
  ```

- Access to service
  - With `port-forward`:

    ```bash
    kubectl port-forward svc/${component} 
    ```

### Develop without container

> You can install these Java tools by [SDKMAN!](https://sdkman.io)
>
> ```bash
> curl -s "https://get.sdkman.io" | bash
> sdk i java 11.0.10.hs-adpt
> sdk i maven 3.6.3
> ```

- JDK 11.0.10 hotspot
  - use AdoptOpenJDK
- Apache Maven 3.6.3
- docker 20.10.2
- docker-compose 1.28.5

Before you start application, you can start backend database:

```bash
./scripts/docker-compose.sh "${component}db"
```

Run your application:

```bash
./scripts/run-develop-host.sh "${component}"
```

### How to create dummy data

We have some scripts to manipulate datas for development.

#### Post sample articles

```bash
ARTICLE_URL=<article endpoint URL> ./scripts/post-sample-articles.bash
```

- `<article endpoint URL>`
  - Article service endpoint URL fomatted `<scheme>://<host>[:<port>]`
  - Default: `http://localhost:8081`

#### Create dummy access record

```bash
ARTICLE_URL=<article endpoint URL> ACCESSCOUNT_URL=<accesscount endpoint URL> ./scripts/dummy-access-at.bash <access date>
```

- `<article endpoint URL>`
  - Article service endpoint URL fomatted `<scheme>://<host>[:<port>]`
  - Default: `http://localhost:8081`
- `<accesscount endpoint URL>`
  - Accesscount service endpoint URL fomatted `<scheme>://<host>[:<port>]`
  - Default: `http://localhost:8082`
- `<access date>`
  - Date for dummy access. Formatted `YYYY-MM-DD`
  - Website shows yesterday's ranking. If you want to create dummy data to show website's ranking, you need to specify yesterday.
  - No default value.

#### Update ranking

```bash
RANK_URL=<rank endpoint URL> ./scripts/update-daily-rank.bash <ranking date>
```

- `<rank endpoint URL>`
  - Rank service endpoint URL fomatted `<scheme>://<host>[:<port>]`
  - Default: `http://localhost:8083`
- `<ranking date>`
  - Date for updating ranking. Formatted `YYYY-MM-DD`
  - Website shows yesterday's ranking. If you want to create dummy data to show website's ranking, you need to specify yesterday.
  - No default value.

## Troubleshoot memo

### MSYS2/Git Bash(Git for Windows)

- Do not use `MSYS2_ARG_CONV_EXCL` or disable that value on top of script like `mvn`/`mvnw`
  - Simple solution: remove environment variable `MSYS2_ARG_CONV_EXCL` from system, `.bashrc`, `.profile`, and so on.
  - If you want to use `MSYS2_ARG_CONV_EXCL='*'` or like it for some other reason, you can disable the variable on top of script. Add `export` command just after shebang like:

    ```sh
    #!/bin/sh
    export MSYS2_ARG_CONV_EXCL=''

    # ...
    ```

- Character encoding error(文字化け)
  - Many java tools output the error message to STDERR not STDOUT. If you use `iconv` command, may need redirect not only STDOUT but also STDERR to `iconv` like:

    ```bash
    mvn install 2>&1 | iconv -f sjis -t utf-8
    ```

## Change Log

|    日  付    |                                                                                                                    |
|:----------:|:---------------------------------------------------------------------------------------------------------------------|
| 2021/08/30 | 各アプリケーションのログに出力しているパスが間違っていたものを修正                                                   |
| 2021/08/30 | CI構成(.gitlab-ci.yml)を追加                                                                                         |
| 2021/08/30 | 外部からのヘルスチェック用エンドポイントをarticle accesscount rank に追加                                            |
| 2021/12/24 | CVE-2021-44228(通称Log4Shell)とその関連脆弱性のあるバージョンのLog4jを誤って取り込まないために安全なバージョンを指定 |
| 2021/12/24 | CVE-2021-42550(別名 LOGBACK-1591)を回避するため修正されたバージョンをlogbackに指定                                   |
| 2022/04/19 | CVE-2022-22965(Spring4Shell/SpringShell)を回避するためSpringBootをアップグレード                                     |
