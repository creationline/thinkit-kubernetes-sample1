package com.creationline.k8sthinkit.sample1.article;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest(properties="spring.profiles.active=test")
class ArticleApplicationTests {

	@Test
	void contextLoads() {
	}

}
