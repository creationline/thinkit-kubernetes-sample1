/**
 * エンティティを読み取り/書き込みするためのコンバータを収めるパッケージ
 */
package com.creationline.k8sthinkit.sample1.accesscount.repository.entity.converter;
