package com.creationline.k8sthinkit.sample1.accesscount;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest(properties="spring.profiles.active=test")
class AccesscountApplicationTests {

	@Test
	void contextLoads() {
	}

}
